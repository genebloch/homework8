import { createElement } from './helpers/domHelper'; 

export function createFighter(fighter: Fighter, 
  handleClick: (ev: Event, fighter: Fighter) => Promise<void>, 
  selectFighter: (ev: Event, fighter: Fighter) => Promise<void>): HTMLElement {
  
  const nameElement = createName(fighter.name);
  const imageElement = createImage(fighter.source);
  const checkboxElement = createCheckbox();
  const fighterContainer = createElement({ tagName: 'div', className: 'fighter' });
  
  fighterContainer.append(imageElement, nameElement, checkboxElement);

  const preventCheckboxClick = (ev: Event) => ev.stopPropagation();
  const onCheckboxClick = (ev: Event) => selectFighter(ev, fighter);
  const onFighterClick = (ev: Event) => handleClick(ev, fighter);

  fighterContainer.addEventListener('click', onFighterClick, false);
  checkboxElement.addEventListener('change', onCheckboxClick, false);
  checkboxElement.addEventListener('click', preventCheckboxClick , false);

  return fighterContainer;
}

function createName(name: string): HTMLElement {
  const nameElement = createElement({ tagName: 'span', className: 'name' });
  nameElement.innerText = name;

  return nameElement;
}

function createImage(source: string): HTMLImageElement {
  const attributes = { src: source };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes }) as HTMLImageElement;

  return imgElement;
}

function createCheckbox(): HTMLElement {
  const label = createElement({ tagName: 'label', className: 'custom-checkbox' });
  const span = createElement({ tagName: 'span', className: 'checkmark' });
  const attributes = { type: 'checkbox' };
  const checkboxElement = createElement({ tagName: 'input', attributes });

  label.append(checkboxElement, span);
  return label;
}