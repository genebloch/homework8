import { callApi } from '../helpers/apiHelper';

export async function getFighters(): Promise<Fighter[]> {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');
    
    return apiResult as Fighter[];
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: string): Promise<Fighter> {
  try {
    const endpoint = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, 'GET');
    
    return apiResult as Fighter;
  } catch (error) {
    throw error;
  }
}
