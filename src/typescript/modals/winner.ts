import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export function showWinnerModal(fighter: Fighter): void {
  const title = `${fighter.name} won`;
  const bodyElement = createWinnerDetails(fighter);
  showModal({ title, bodyElement });
}

function createWinnerDetails(fighter: Fighter): HTMLElement {
  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const imageElement = createElement({ tagName: 'img', className: 'fighter-img' }) as HTMLImageElement;

  imageElement.src = fighter.source;
  
  fighterDetails.append(imageElement);

  return fighterDetails;
}