export function fight(firstFighter, secondFighter) {
    let firstFighterHealth = firstFighter.health;
    let secondFighterHealth = secondFighter.health;
    let i = 1;
    while (true) {
        firstFighterHealth -= getDamage(secondFighter, firstFighter);
        secondFighterHealth -= getDamage(firstFighter, secondFighter);
        console.log(`kick ${i}:`);
        console.log(`\t${firstFighter.name}: ${firstFighterHealth}`);
        console.log(`\t${secondFighter.name}: ${secondFighterHealth}`);
        if (firstFighterHealth <= 0 || secondFighterHealth <= 0)
            break;
        i++;
    }
    return firstFighterHealth > 0 ? firstFighter : secondFighter;
}
export function getDamage(attacker, enemy) {
    let damage = getHitPower(attacker) - getBlockPower(enemy);
    return damage > 0 ? damage : 0;
}
export function getHitPower(fighter) {
    let criticalHitChance = Math.random() + 1;
    let power = fighter.attack * criticalHitChance;
    return power;
}
export function getBlockPower(fighter) {
    let dodgeChance = Math.random() + 1;
    let power = fighter.defense * dodgeChance;
    return power;
}
