import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
export function showFighterDetailsModal(fighter) {
    const title = 'Fighter info';
    const bodyElement = createFighterDetails(fighter);
    showModal({ title, bodyElement });
}
function createFighterDetails(fighter) {
    const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
    const fighterImage = createElement({ tagName: 'div', className: 'fighter-image-wrapper' });
    const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
    const healthElement = createElement({ tagName: 'span', className: 'fighter-health' });
    const attackElement = createElement({ tagName: 'span', className: 'fighter-attack' });
    const defenseElement = createElement({ tagName: 'span', className: 'fighter-defense' });
    const imageElement = createElement({ tagName: 'img', className: 'fighter-img' });
    nameElement.innerText = `Name: ${fighter.name}`;
    healthElement.innerText = `\nHealth: ${fighter.health}`;
    attackElement.innerText = `\nAttack: ${fighter.attack}`;
    defenseElement.innerText = `\nDefense: ${fighter.defense}`;
    imageElement.src = fighter.source;
    fighterImage.appendChild(imageElement);
    fighterDetails.append(nameElement, healthElement, attackElement, defenseElement, fighterImage);
    return fighterDetails;
}
