import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
export function showWinnerModal(fighter) {
    const title = `${fighter.name} won`;
    const bodyElement = createWinnerDetails(fighter);
    showModal({ title, bodyElement });
}
function createWinnerDetails(fighter) {
    const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
    const imageElement = createElement({ tagName: 'img', className: 'fighter-img' });
    imageElement.src = fighter.source;
    fighterDetails.append(imageElement);
    return fighterDetails;
}
