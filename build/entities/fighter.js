"use strict";
class Fighter {
    constructor(_id, name, health, attack, defense, source) {
        this._id = _id;
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
        this.source = source;
    }
}
